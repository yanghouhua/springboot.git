package cn.edu.main;

import cn.edu.spi.Animal;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * 测试类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/4 15:15
 */
public class SpiTest {

    public static void main(String[] args) {
        ServiceLoader<Animal> animals = ServiceLoader.load(Animal.class);
        Iterator<Animal> iter = animals.iterator();
        while (iter.hasNext()){
            Animal animal = iter.next();
            animal.hello("Bob");
        }
    }
}

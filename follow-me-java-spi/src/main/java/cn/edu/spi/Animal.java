package cn.edu.spi;

/**
 * spi 案例：接口
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/4 15:10
 */
public interface Animal {

    /**
     * 动物喊话
     * @param name
     */
    void hello(String name);
}

package cn.edu.spi.impl;

import cn.edu.spi.Animal;

/**
 * spi案例：接口实现 鸟
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/4 15:12
 */
public class Bird  implements Animal{

    /**
     * 动物喊话
     *
     * @param name
     */
    @Override
    public void hello(String name) {
        System.out.println("I am small small small Bird! name is：" +name);
    }
}

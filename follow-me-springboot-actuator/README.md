# springboot项目健康检查actuator

## 1.准备环境

### 1.1.引入actuator依赖

```xml
<!--监控健康检查依赖-->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```



### 1.2.编写配置

```yml
server:
  port: 8080
management:
  server:
    port: 8888
  endpoints:
    web:
      exposure:
        include: '*'
        exclude: beans,env,metrics
  endpoint:
    health:
      show-details: always
info:
  app: follow-me-springboot-actuator
  author: yanghouhua
  date: 20210731
```

配置说明

- management.server.port：健康检查端点开放端口
- management.endpoints.web.exposure.include：配置开放健康检查端点，*表示开放所有
- management.endpoints.web.exposure.exclude：配置哪些端点不开放
- management.endpoint.health.show-details：配置health端点显示详细信息
- info：自定义Info端点信息



## 2.自定义监控指标

在实际项目中，有时候我们可能会有监控业务的需求，springboot给我们提供了一个**HealthIndicator**接口，实现该接口，即可实现自定义监控指标。

```java
/**
 * 自定义健康检查
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/7/26 20:43
 */
@Component
public class EduHealthIndicator implements HealthIndicator{

    public Health health() {
       // return  Health.unknown().withDetail("status",666).build();
       // return  Health.unknown().withDetail("status",666).build();
        return  Health.up().withDetail("status",666).build();
    }
}
```

访问health端点：http://127.0.0.1:8888/actuator/health

![image-20210731123626886](assets/image-20210731123626886.png)


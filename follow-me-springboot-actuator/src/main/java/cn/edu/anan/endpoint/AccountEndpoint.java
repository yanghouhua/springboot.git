package cn.edu.anan.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 开放自定义端点，通过@Selector注解实现参数传递
 * @author  yhh
 */
@Component
@Endpoint(id = "account")
public class AccountEndpoint {

    /**
     * 传递参数
     * @param arg0
     * @return
     */
    @ReadOperation
    public Map<String, Object> accountInfo(@Selector String arg0) {
        Map<String, Object> result = new HashMap<String, Object>(8);
        result.put("accountName", arg0);

        return result;

    }
}

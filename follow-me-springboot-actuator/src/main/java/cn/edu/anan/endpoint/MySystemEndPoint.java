package cn.edu.anan.endpoint;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 自定义端点
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/2 16:15
 */
@Component
@Endpoint(id="system")
public class MySystemEndPoint {

    /**
     * 该端点返回用户名称，与计算机名称
     * @return
     */
    @ReadOperation
    public Map<String, Object> systemInfo() {
        Map<String,Object> result= new HashMap<String,Object>(8);
        // 获取系统信息
        Map<String, String> map = System.getenv();
        // 获取计算机用户名称、计算机名称
        result.put("username",map.get("USERNAME"));
        result.put("computername",map.get("COMPUTERNAME"));

        return result;
    }
}

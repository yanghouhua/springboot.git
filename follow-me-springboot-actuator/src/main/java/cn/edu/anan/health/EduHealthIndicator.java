package cn.edu.anan.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * 自定义健康检查
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/7/26 20:43
 */
@Component
public class EduHealthIndicator implements HealthIndicator{

    public Health health() {
       // return  Health.unknown().withDetail("status",666).build();
       // return  Health.unknown().withDetail("status",666).build();
        return  Health.up().withDetail("status",666).build();
    }
}

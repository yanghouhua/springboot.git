package cn.edu.anan.rule;

/**
 * <p>
 * sentinel 资源
 * </p>
 *
 * @author yanghouhua@foresee.com.cn
 * @since 2021/8/5
 */
public interface MySentinelResource {
    /**
     * 流控资源
     */
    public static final String FLOW_RESOURCE = "flowResource";

    /**
     * 流控资源
     */
    public static final String DEGRADE_RESOURCE = "degradeResource";

}

package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *启动类
 * </p>
 *
 * @author yanghouhua@foresee.com.cn
 * @since 2021/8/5
 */
@SpringBootApplication
public class FollowMeSentinelApplication {

    public static void main(String[] args) {
        SpringApplication.run(FollowMeSentinelApplication.class, args);
    }

}

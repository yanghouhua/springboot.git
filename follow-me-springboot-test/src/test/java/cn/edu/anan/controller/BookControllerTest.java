package cn.edu.anan.controller;

import cn.edu.anan.base.WebBaseTester;
import cn.edu.anan.client.AccountClient;
import cn.edu.anan.entity.Account;
import cn.edu.anan.entity.Book;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * controller层测试类
 * mockito框架使用参考：https://segmentfault.com/a/1190000006746409
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 16:52
 */
@Slf4j
public class BookControllerTest extends WebBaseTester{

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private AccountClient accountClient;

    /**
     * 初始化测试数据
     */
    @Autowired
    private BookController bookController;

    @Before
    public void setUp(){
        Book book = Book.builder()
                .id(1)
                .author("xy")
                .bookName("java")
                .publishDate(LocalDate.now())
                .build();
        Book book2 = Book.builder()
                .id(2)
                .author("lj")
                .bookName("spring")
                .publishDate(LocalDate.now())
                .build();

        bookController.save(book);
        bookController.save(book2);
    }

    /**
     * 测试查询图书列表
     * @throws Exception
     */
    @Test
    public void testListBook() throws Exception{
        // 查询结果
        Account account = Account.builder()
                .id("666")
                .name("xy")
                .email("xy@126.com")
                .build();
        // mock
        when(accountClient.findById(anyString()))
                .thenReturn(account);

        // mock查询图书列表
        MvcResult mvcResult = mockMvc.perform(get("/book/list"))
                .andExpect(status().isOk())
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
        log.info("测试结果：{}", contentAsString);

    }
}

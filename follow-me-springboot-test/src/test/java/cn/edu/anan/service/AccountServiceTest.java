package cn.edu.anan.service;

import cn.edu.anan.base.BaseTester;
import cn.edu.anan.entity.Account;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * service层测试
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 16:12
 */
@Slf4j
public class AccountServiceTest extends BaseTester{

    @Autowired
    private AccountService accountService;

    @Test
    public void testService(){
        // 准备数据
        Account account = Account.builder()
                .name("yhh")
                .email("yhh@126.com")
                .build();
        // 保存
        accountService.save(account);

        // 查询
        Account accountById = accountService.findAccountById(account.getId());
        log.info("查询到数据：{}", accountById);

    }

}

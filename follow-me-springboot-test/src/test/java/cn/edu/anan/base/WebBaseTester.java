package cn.edu.anan.base;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * web测试基类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/4 16:55
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class WebBaseTester extends BaseTester{

}

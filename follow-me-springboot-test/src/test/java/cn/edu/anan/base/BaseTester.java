package cn.edu.anan.base;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试基类
 *  构建命令：mvn clean package
 *  跳过单元测试：
 *      mvn clean package -DskipTests
 *      mvn clean package -Dmaven.test.skip=true
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/4 16:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class BaseTester {

}

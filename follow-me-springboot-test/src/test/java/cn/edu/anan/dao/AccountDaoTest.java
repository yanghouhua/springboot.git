package cn.edu.anan.dao;

import cn.edu.anan.base.BaseTester;
import cn.edu.anan.entity.Account;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * dao层单元测试
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 15:41
 */
@Slf4j
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class AccountDaoTest extends BaseTester{

    @Autowired
    private AccountDao accountDao;

    private Account account;

    /**
     * 初始化数据
     */
    @Before
    public void setUp() {
        log.info(".................setUp..................");
        // 准备账户数据
        account = Account.builder()
                .name("yhh")
                .email("yhh@126.com")
                .build();

    }

    /**
     * 保存数据
     */
    @Test
    public void test001Save(){
        log.info(".................test001Save..................");
        accountDao.save(account);
    }

    /**
     * 查询列表数据
     */
    @Test
    public void test002ListAccount(){
        log.info(".................test002ListAccount..................");
        // 查询
        Iterable<Account> all = accountDao.findAll();
        all.forEach(acc ->{
            log.info("查询到数据：{}", acc);
        });

    }

}

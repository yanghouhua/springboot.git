--账户表
CREATE TABLE IF NOT EXISTS account (
                                     id VARCHAR(255),
                                     email VARCHAR(255) NOT NULL,
                                     name VARCHAR(255) NOT NULL default '',
                                     PRIMARY KEY (id)
) ENGINE=InnoDB;

-- time-zone issue reference
-- https://blog.csdn.net/CHS007chs/article/details/81348291

-- 图书表
CREATE TABLE IF NOT EXISTS book (
  id int(11) NOT NULL,
  author varchar(255) DEFAULT NULL,
  book_name varchar(255) DEFAULT NULL,
  publish_date date DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

package cn.edu.anan.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

/**
 *图书模型
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/4 11:45
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "自增主键",dataType = "Integer", name = "id", example = "1")
    private Integer id;
    @ApiModelProperty(value = "图书名称",dataType = "String", name = "bookName", example = "java编程思想")
    private String bookName;
    @ApiModelProperty(value = "作者",dataType = "String", name = "author", example = "lisi")
    private String author;
    @ApiModelProperty(value = "出版日期",dataType = "LocalDate", name = "publishDate", example = "2021-10-03")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate publishDate;

}

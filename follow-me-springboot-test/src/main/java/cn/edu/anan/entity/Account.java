package cn.edu.anan.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 账户模型
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 15:26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Account {
    @Id
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @GeneratedValue(generator = "system-uuid")
    @ApiModelProperty(value = "主键",dataType = "String", name = "id", example = "uuid")
    private String id;
    @ApiModelProperty(value = "姓名",dataType = "String", name = "name", example = "zhangs")
    private String name;
    @ApiModelProperty(value = "邮箱",dataType = "String", name = "email", example = "zhangs@126.com")
    private String email;

}
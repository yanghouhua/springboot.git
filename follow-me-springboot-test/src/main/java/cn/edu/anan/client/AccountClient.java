package cn.edu.anan.client;

import cn.edu.anan.entity.Account;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 账户feign client
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/4 13:15
 */
@FeignClient(name = "springboot-test", path = "/account", url = "http://127.0.0.1:8080")
public interface AccountClient {

    /**
     * 根据Id查询账户
     * @param id
     * @return
     */
    @GetMapping("findById")
    Account findById(@RequestParam(value = "id") String id);
}

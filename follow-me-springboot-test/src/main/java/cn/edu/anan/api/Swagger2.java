package cn.edu.anan.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2 配置类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 15:26
 */
@Configuration
@EnableSwagger2
public class Swagger2 {


    /**
     * 创建API应用
     *  1.apiInfo方法：增加API相关信息
     *  2.select方法：返回ApiSelectorBuilder实例，用于控制哪些接口暴露给Swagger展现
     *  3.采用扫描的包路径，指定建立API的目录
     * @return Docket
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(true)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.edu.anan"))
                .paths(PathSelectors.any())
                .build();

    }


    /**
     * 创建api基本信息
     * api访问地址：http://项目实际地址/swagger-ui.html
     * @return  ApiInfo
     */
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("跟我学spring boot系列：api接口文档")
                .version("1.0")
                .description("Swagger2管理api接口文档实践")
                .contact(new Contact("开源中国-yhhitalll个人技术博客", "https://my.oschina.net/u/4450329", "331407008@qq.com"))
                .build();

    }
}
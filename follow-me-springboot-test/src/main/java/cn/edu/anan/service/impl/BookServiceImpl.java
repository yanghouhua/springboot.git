package cn.edu.anan.service.impl;

import cn.edu.anan.dao.BookDao;
import cn.edu.anan.entity.Book;
import cn.edu.anan.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 图书service实现
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/4 11:59
 */
@Service
public class BookServiceImpl implements BookService{

    @Autowired
    private BookDao bookDao;

    /**
     * 保存图书
     *
     * @param book
     * @return
     */
    @Override
    public Book save(Book book) {
        return  bookDao.save(book);
    }

    /**
     * 查询全部图书
     *
     * @return
     */
    @Override
    public List<Book> listAllBook() {
        Iterable<Book> all = bookDao.findAll();
        List<Book> list = new ArrayList<>();
        all.forEach(book -> {
            list.add(book);
        });
        return list;
    }
}

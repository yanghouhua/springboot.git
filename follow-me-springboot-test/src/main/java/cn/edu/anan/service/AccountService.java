package cn.edu.anan.service;

import cn.edu.anan.entity.Account;

/**
 * 账户接口
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 15:22
 */
public interface AccountService {

    /**
     * 保存账户
     * @param account
     * @return
     */
    Integer save(Account account);

    /**
     * 根据账户Id，查询账户
     * @param id
     * @return
     */
    Account findAccountById(String id);
}

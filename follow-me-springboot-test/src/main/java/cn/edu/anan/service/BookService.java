package cn.edu.anan.service;

import cn.edu.anan.entity.Book;

import java.util.List;

/**
 * 图书service接口
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/4 11:57
 */
public interface BookService {

    /**
     * 保存图书
     * @param book
     * @return
     */
    Book save(Book book);

    /**
     * 查询全部图书
     * @return
     */
    List<Book> listAllBook();
}

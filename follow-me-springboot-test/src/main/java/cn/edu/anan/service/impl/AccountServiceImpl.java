package cn.edu.anan.service.impl;

import cn.edu.anan.dao.AccountDao;
import cn.edu.anan.entity.Account;
import cn.edu.anan.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * 账户实现类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 15:23
 */
@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountDao accountDao;

    /**
     * 保存账户
     *
     * @param account
     * @return
     */
    @Override
    public Integer save(Account account) {
        accountDao.save(account);
        return 1;
    }

    /**
     * 根据账户Id，查询账户
     *
     * @param id
     * @return
     */
    @Override
    public Account findAccountById(String id) {
        Optional<Account> byId = accountDao.findById(id);
        return byId.orElse(new Account());
    }
}

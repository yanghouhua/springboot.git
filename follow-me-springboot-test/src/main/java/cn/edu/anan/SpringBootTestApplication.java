package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 启动类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 15:15
 */
@SpringBootApplication
@EnableFeignClients(basePackages = {"cn.edu.anan.client"})
public class SpringBootTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTestApplication.class, args);
    }
}

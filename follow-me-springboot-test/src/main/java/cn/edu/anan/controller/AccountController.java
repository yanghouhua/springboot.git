package cn.edu.anan.controller;

import cn.edu.anan.entity.Account;
import cn.edu.anan.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 账户controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 15:26
 */
@RestController
@RequestMapping("account")
@Api(tags = "账户rest接口")
public class AccountController {

    @Autowired
    private AccountService accountService;

    /**
     * 保存账户
     * @param account
     * @return
     */
    @PostMapping("save")
    @ApiOperation(value = "保存账户")
    public String save(@RequestBody Account account){
        // 保存
        accountService.save(account);

        return "ok";
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    @GetMapping("findById")
    @ApiOperation(value = "根据Id查询账户")
    public Account findById(String id){
        // 查询
        return accountService.findAccountById(id);
    }
}

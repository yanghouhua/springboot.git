package cn.edu.anan.controller;

import cn.edu.anan.client.AccountClient;
import cn.edu.anan.entity.Account;
import cn.edu.anan.entity.Book;
import cn.edu.anan.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 图书controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/4 12:01
 */
@RestController
@RequestMapping("book")
@Api(tags = "图书rest接口")
@Slf4j
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private AccountClient accountClient;

    /**
     * 保存图书
     * @param book
     * @return
     */
    @PostMapping("save")
    @ApiOperation(value = "保存图书")
    public Book save(Book book){
        return bookService.save(book);
    }

    /**
     * 查询图书列表
     * @return
     */
    @GetMapping("list")
    @ApiOperation(value = "查询图书列表")
    public List<Book> list(){
        // 查询账户信息
        Account account = accountClient.findById("4028b8817c450f65017c4511fdab0000");
        log.info("查询到账户信息：{}", account);
        return bookService.listAllBook();
    }
}

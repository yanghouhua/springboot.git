package cn.edu.anan.dao;

import cn.edu.anan.entity.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 账户dao
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/3 15:20
 */
@Repository
public interface AccountDao extends CrudRepository<Account, String>{

}

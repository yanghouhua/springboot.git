package cn.edu.anan.dao;

import cn.edu.anan.entity.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 图书Dao
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/10/4 11:52
 */
@Repository
public interface BookDao extends CrudRepository<Book, Integer>{

}

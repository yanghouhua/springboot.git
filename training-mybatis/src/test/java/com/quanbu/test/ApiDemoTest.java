package com.quanbu.test;

import com.quanbu.entity.Account;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * 传统api开发方式
 * <br>CreateDate 2022-01-08 9:05
 *
 * @author yanghouhua
 * @since 1.0.0
 */
public class ApiDemoTest {

    @Test
    public void selectAllTest() throws  Exception{

        // 初始化环境，拿到sqlSession
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 执行数据库操作
        List<Account> list = sqlSession.selectList("test.selectAll");
        list.forEach(o ->{
            System.out.println(o);
        });

        // 释放资源
        sqlSession.close();

    }
}

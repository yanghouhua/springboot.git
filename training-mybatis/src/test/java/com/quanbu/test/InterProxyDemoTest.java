package com.quanbu.test;

import com.quanbu.entity.Account;
import com.quanbu.mapper.AccountMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * mapper代理接口开发方式
 * <br>CreateDate 2022-01-08 9:22
 *
 * @author yanghouhua
 * @since 1.0.0
 */
public class InterProxyDemoTest {

    @Test
    public void findAllTest() throws  Exception{
        // 初始化环境，拿到sqlSession
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 拿到接口代理对象
        AccountMapper mapper = sqlSession.getMapper(AccountMapper.class);
        List<Account> list = mapper.findAll();
        list.forEach(o ->{
            System.out.println(o);
        });

        // 释放资源
        sqlSession.close();

    }
}

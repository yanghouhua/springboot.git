package com.quanbu.entity;

import lombok.Data;

/**
 * 账户实体
 * <br>CreateDate 2022-01-08 9:01
 *
 * @author yanghouhua
 * @since 1.0.0
 */
@Data
public class Account {
    private Integer id;
    private String name;
    private Float money;
}

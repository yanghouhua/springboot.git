package com.quanbu.mapper;

import com.quanbu.entity.Account;

import java.util.List;

/**
 * 账户mapper接口
 * <br>CreateDate 2022-01-08 9:17
 *
 * @author yanghouhua
 * @since 1.0.0
 */
public interface AccountMapper {
    /**
     * 查询全部账户列表
     * @return
     */
    List<Account> findAll();
}

package cn.edu.anan.config;

import cn.edu.anan.Animal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * spi 配置类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/4 15:59
 */
@Configuration
public class SpiConfiguration {

    @Bean
    public Animal animal(){
        Animal animal = null;

        ServiceLoader<Animal> animals = ServiceLoader.load(Animal.class);
        Iterator<Animal> iter = animals.iterator();
        while (iter.hasNext()){
            animal = iter.next();
            break;
        }

        return animal;
    }
}

package cn.edu.anan.controller;

import cn.edu.anan.Animal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * spi controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/4 15:55
 */
@RestController
@RequestMapping("spi")
@Slf4j
public class SpiController {

    @Autowired
    private Animal animal;

    @RequestMapping("test")
    public String test(){
        animal.hello();
        return animal.getClass().getName();
    }
}

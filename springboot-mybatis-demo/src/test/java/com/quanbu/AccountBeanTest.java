package com.quanbu;

import com.quanbu.controller.AccountController;
import com.quanbu.entity.Account;
import com.quanbu.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * account单元测试
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2022/2/6 8:31
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SpringbootMybatisDemoApplication.class)
public class AccountBeanTest {

    @InjectMocks
    private AccountController accountController;

    @Mock
    private AccountService accountService;

    private MockMvc mockMvc;

    @Before
    public void init(){
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
    }

    @Test
    public void testFindAll() throws  Exception{
        Account account = new Account();
        account.setId(1);
        account.setName("xxx");
        account.setMoney(666f);

        List<Account> list = new ArrayList<>();
        list.add(account);

        when(accountService.findAll(anyInt(),anyInt())).thenReturn(list);

        // 模拟接口调用
        ResultActions perform = this.mockMvc.perform(MockMvcRequestBuilders.get("/account/list?pageNum=1&pageSize=2"));

        // 验证接口响应
        perform.andExpect(MockMvcResultMatchers.status().isOk());
        System.out.println(perform.andReturn().getResponse().getContentAsString());

    }
}

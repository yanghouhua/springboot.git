package com.quanbu;

import com.quanbu.entity.Account;
import com.quanbu.mapper.AccountMapper;
import com.quanbu.service.impl.AccountServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * service单元测试
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2022/2/6 8:07
 */
@RunWith(PowerMockRunner.class)
public class AccountServiceImplTest {

    @Mock
    private AccountMapper accountMapper;

    @InjectMocks
    private AccountServiceImpl accountService;

    @Before
    public void init(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testList(){
        Account account = new Account();
        account.setId(1);
        account.setName("xxx");
        account.setMoney(666f);

        List<Account> list = new ArrayList<>();
        list.add(account);

        when(accountMapper.findAll()).thenReturn(list);
        List<Account> result = accountService.findAll(1, 2);
        System.out.println(result);
    }
}

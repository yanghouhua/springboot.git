package com.quanbu.controller;

import com.quanbu.entity.Account;
import com.quanbu.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 账户controller
 * <br>CreateDate 2022-01-08 20:53
 *
 * @author yanghouhua
 * @since 1.0.0
 */
@RestController
@RequestMapping("account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    /**
     * 查询全部账户列表
     * @return
     */
    @RequestMapping("list")
    public List<Account> findAll(Integer pageNum, Integer pageSize){
        return accountService.findAll(pageNum,pageSize);
    }
}

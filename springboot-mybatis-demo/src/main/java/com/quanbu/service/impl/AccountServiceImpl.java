package com.quanbu.service.impl;

import com.github.pagehelper.PageHelper;
import com.quanbu.entity.Account;
import com.quanbu.mapper.AccountMapper;
import com.quanbu.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 账户service实现类
 * <br>CreateDate 2022-01-08 20:52
 *
 * @author yanghouhua
 * @since 1.0.0
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    /**
     * 分页查询账户列表
     * @param pageNum 页码
     * @param pageSize 页大小
     * @return
     */
    @Override
    public List<Account> findAll(Integer pageNum, Integer pageSize) {
        // 设置分页
        PageHelper.startPage(pageNum,pageSize);
        // 查询数据
        return accountMapper.findAll();
    }
}

package com.quanbu.service;

import com.quanbu.entity.Account;

import java.util.List;

/**
 * 账户service
 * <br>CreateDate 2022-01-08 20:51
 *
 * @author yanghouhua
 * @since 1.0.0
 */
public interface AccountService {

    /**
     * 查询全部账户列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<Account> findAll(Integer pageNum, Integer pageSize);
}

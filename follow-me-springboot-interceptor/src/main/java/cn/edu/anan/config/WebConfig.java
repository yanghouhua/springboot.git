package cn.edu.anan.config;

import cn.edu.anan.interceptor.LimitInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web 配置
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/12 8:31
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private LimitInterceptor limitInterceptor;

    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加限流拦截器
        registry.addInterceptor(limitInterceptor);

    }
}
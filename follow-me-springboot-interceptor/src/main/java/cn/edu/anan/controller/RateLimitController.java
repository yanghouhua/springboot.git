package cn.edu.anan.controller;

import cn.edu.anan.interceptor.AccessLimit;
import cn.edu.anan.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 限流controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/12 8:31
 */
@RestController
public class RateLimitController {

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 测试方法
     * @return
     */
    @RequestMapping("noLimit")
    public String noLimit(){
        // 测试redis工具
        redisUtil.inCr("rate:limit:test", 1L);
        return "no limit.";
    }

    /**
     * 需要限流方法
     * @return
     */
    @RequestMapping("needLimit")
    @AccessLimit(seconds=60, maxCount=5)
    public String rateLimit(){

        return "need limit.";
    }
}

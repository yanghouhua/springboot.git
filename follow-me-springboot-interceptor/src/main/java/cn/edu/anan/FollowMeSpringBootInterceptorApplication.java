package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/12 8:13
 */
@SpringBootApplication
public class FollowMeSpringBootInterceptorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FollowMeSpringBootInterceptorApplication.class, args);
    }
}

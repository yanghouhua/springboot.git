package cn.edu.thirtybean;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 加载第三方bean 2
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/1 14:05
 */
@Configuration
@Slf4j
public class ThirtyBeanConfiguration2 {

    @Bean
    public String thirtyConfig2(){
        log.info("启动加载第三方bean2对象...start.{}", ThirtyBeanConfiguration2.class);
        return "thirtyBean2 ok.";
    }
}

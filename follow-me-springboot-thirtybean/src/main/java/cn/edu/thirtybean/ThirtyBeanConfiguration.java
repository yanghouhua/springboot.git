package cn.edu.thirtybean;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 加载第三方bean
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/1 14:05
 */
@Configuration
@Slf4j
public class ThirtyBeanConfiguration {

    @Bean
    public String thirtyConfig(){
        log.info("启动加载第三方bean对象...start.{}", ThirtyBeanConfiguration.class);
        return "thirtyBean ok.";
    }
}

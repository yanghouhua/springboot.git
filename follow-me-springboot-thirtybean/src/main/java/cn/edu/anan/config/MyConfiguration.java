package cn.edu.anan.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 项目自身配置类，即在启动类扫描的package范围
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/1 13:59
 */
@Configuration
@Slf4j
public class MyConfiguration {

    @Bean
    public String autoConfig(){
        log.info("启动加载自身bean对象...start.{}", MyConfiguration.class);
        return "autoConfig ok.";
    }
}

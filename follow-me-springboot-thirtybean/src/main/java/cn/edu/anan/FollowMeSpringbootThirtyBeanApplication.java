package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/1 13:58
 */
@SpringBootApplication
public class FollowMeSpringbootThirtyBeanApplication {

    public static void main(String[] args) {
        SpringApplication.run(FollowMeSpringbootThirtyBeanApplication.class, args);
    }
}

package cn.edu.anan.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * jasypt配置类
 */
@Configuration
@ConfigurationProperties("my.ds")
@Data
public class JasyptConfiguration {

    private String url;
    private String username;
    private String password;

}
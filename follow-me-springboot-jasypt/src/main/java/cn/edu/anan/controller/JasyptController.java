package cn.edu.anan.controller;

import cn.edu.anan.aop.JasyptMethod;
import cn.edu.anan.config.JasyptConfiguration;
import cn.edu.anan.vo.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller
 */
@RestController
@RequestMapping("jasypt")
@Slf4j
public class JasyptController {

    /**
     * 请求参数脱敏处理
     * @param vo
     * @return
     */
    @RequestMapping("encrypt")
    @JasyptMethod
    public UserVO encrypt(UserVO vo){
        log.info("2.脱敏后的数据：{}", vo);
        return vo;
    }

    @Autowired
    private StringEncryptor stringEncryptor;

   @Autowired
   private JasyptConfiguration jasyptConfiguration;

    @RequestMapping("ok")
    public String ok(){
        log.info("数据源连接属性内容：{}", jasyptConfiguration);
        return "ok";
    }


    /**
     * 加密
     * @param content
     * @return
     */
    @RequestMapping("encrypt/{content}")
    public String encrypt(@PathVariable("content") String content) {
        String encrypt = stringEncryptor.encrypt(content);
        log.info("通过jasypt加密，明文：{}，密文：{}", content, encrypt);
        return encrypt;
    }
}
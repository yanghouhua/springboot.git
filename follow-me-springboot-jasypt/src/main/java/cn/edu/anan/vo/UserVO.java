package cn.edu.anan.vo;

import cn.edu.anan.aop.JasyptField;
import lombok.Data;

/**
 * 用户VO
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/1 9:21
 */
@Data
public class UserVO {

    private Long userId;
    private String userName;
    private Integer age;

    /**
     * 需要脱敏处理的成员变量
     */
    @JasyptField
    private String phone;
    @JasyptField
    private String email;
}

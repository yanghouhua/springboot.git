package cn.edu.anan.email;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 发送邮件controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/7/24 6:39
 */
@RestController
@RequestMapping("mail")
public class MailController {

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private MailProperties mailProperties;

    @Autowired
    private MailConfiguration mailConfiguration;

    @Autowired
    private Configuration freemarkerConfiguration;

    /**
     * 发送简单文本邮件
     *
     * @return ok
     */
    @GetMapping("/text")
    public String textEmail() {
        // 邮件信息：主题、内容
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("清晨问候!");
        message.setText("一日之计在于晨，你好吗?");

        // 发件人
        message.setFrom(this.mailProperties.getUsername());
        // 收件人
        message.setTo(mailConfiguration.getTos());
        // 抄送人
        message.setCc(mailConfiguration.getCcs());

        // 发送
        this.javaMailSender.send(message);

        return "ok";
    }

    /**
     * 发送html格式邮件
     *
     * @return ok
     * @throws MessagingException
     */
    @GetMapping("/html")
    public String htmlEmail() throws MessagingException {

        // 邮件信息：主题、内容
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message);
        messageHelper.setSubject("清晨问候! for html");
        messageHelper.setText("<h1>一日之计在于晨，你好吗?</h1>", true);

        // 发件人
        messageHelper.setFrom(this.mailProperties.getUsername());
        // 收件人
        messageHelper.setTo(mailConfiguration.getTos());
        // 抄送人
        messageHelper.setCc(mailConfiguration.getCcs());

        // 发送
        this.javaMailSender.send(message);

        return "ok";
    }

    /**
     * 带附件的邮件
     *
     * @return ok
     * @throws MessagingException
     */
    @GetMapping("/attach")
    public String attachEmail() throws MessagingException {
        // 邮件信息：主题、内容、附件
        MimeMessage message = this.javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);

        messageHelper.setSubject("清晨问候! for attach");
        messageHelper.setText("<h1>一日之计在于晨，你好吗?</h1>", true);
        // 附件
        messageHelper.addAttachment("me.jpg", new ClassPathResource("me.jpg"));

        // 发件人
        messageHelper.setFrom(this.mailProperties.getUsername());
        // 收件人
        messageHelper.setTo(mailConfiguration.getTos());
        // 抄送人
        messageHelper.setCc(mailConfiguration.getCcs());

        // 发送
        this.javaMailSender.send(message);

        return "ok";
    }

    /**
     * 通过模板渲染邮件内容
     *
     * @return ok
     * @throws MessagingException
     */
    @GetMapping("/ftl")
    public String ftlEmail() throws MessagingException, IOException, TemplateException {
        // 邮件信息：主题、内容
        MimeMessage message = this.javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
        messageHelper.setSubject("ftl模板渲染邮件内容");

        // 邮件内容数据
        Map<String, Object> emailData = new HashMap<>();
        emailData.put("user", "anan");
        emailData.put("event", "厚德教育新闻发布会!");

        // 通过freeMarker渲染邮件内容
        String content = FreeMarkerTemplateUtils.processTemplateIntoString(
                this.freemarkerConfiguration.getTemplate("mail.ftl"), emailData);

        // 设置邮件内容
        messageHelper.setText(content, true);

        // 发件人
        messageHelper.setFrom(this.mailProperties.getUsername());
        // 收件人
        messageHelper.setTo(mailConfiguration.getTos());
        // 抄送人
        messageHelper.setCc(mailConfiguration.getCcs());

        // 发送
        this.javaMailSender.send(message);

        return "ok";
    }

}

package cn.edu.anan.email;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 邮件配置类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/7/24 6:57
 */
@Component
@ConfigurationProperties(prefix = "edu.mail")
@Data
public class MailConfiguration {

    /**
     * 收件人
     */
    private String[] tos;
    /**
     * 抄送人
     */
    private String[] ccs;

}

package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FollowMeSpringbootEmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(FollowMeSpringbootEmailApplication.class, args);
	}

}

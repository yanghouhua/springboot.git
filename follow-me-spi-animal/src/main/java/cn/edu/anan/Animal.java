package cn.edu.anan;

/**
 * spi接口
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/4 16:09
 */
public interface Animal {
    /**
     * 接口方法
     */
    void hello();
}

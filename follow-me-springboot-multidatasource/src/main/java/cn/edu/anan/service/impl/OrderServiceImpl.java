package cn.edu.anan.service.impl;

import cn.edu.anan.dao.OrderMapper;
import cn.edu.anan.entity.Order;
import cn.edu.anan.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 订单service实现类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 17:40
 */
@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 写入单个订单
     *
     * @param order
     * @return
     */
    @Override
    public Integer insertOne(Order order) {
        return orderMapper.insertOne(order);
    }
}

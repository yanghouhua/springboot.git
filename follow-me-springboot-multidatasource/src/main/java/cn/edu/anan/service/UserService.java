package cn.edu.anan.service;

import cn.edu.anan.entity.User;

import java.util.List;

/**
 * 用户service接口
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 17:03
 */
public interface UserService {

    /**
     * 查询全部用户列表
     * @return
     */
    List<User> selectAll(User user);
}

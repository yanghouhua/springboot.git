package cn.edu.anan.service;

import cn.edu.anan.entity.Order;

/**
 * 订单service
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 17:39
 */
public interface OrderService {

    /**
     * 写入单个订单
     * @param order
     * @return
     */
    Integer insertOne(Order order);
}

package cn.edu.anan.service.impl;

import cn.edu.anan.aop.DataSourceSwitcher;
import cn.edu.anan.dao.UserMapper;
import cn.edu.anan.entity.User;
import cn.edu.anan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户service实现类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 17:05
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    /**
     * 查询全部用户列表
     *
     * @return
     */
    @Override
    @DataSourceSwitcher(value = "read")
    public List<User> selectAll(User user) {
        return userMapper.selectAll();
    }

}

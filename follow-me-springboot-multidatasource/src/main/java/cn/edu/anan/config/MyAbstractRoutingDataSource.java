package cn.edu.anan.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 数据源路由，扩展AbstractRoutingDataSource
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 16:05
 */
@Slf4j
public class MyAbstractRoutingDataSource extends AbstractRoutingDataSource{

    /**
     * 返回数据源路由key
     * @return
     */
    @Override
    protected Object determineCurrentLookupKey() {
        String dbKey = DbContextHolder.getDbType();
        if (dbKey == DbContextHolder.WRITE) {
            log.info("当前更新动作，走主库");
            return dbKey;
        }

        log.info("当前读取操作，走从库");
        return DbContextHolder.READ;
    }
}

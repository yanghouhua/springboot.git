package cn.edu.anan.aop;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface DataSourceSwitcher {
    /**
     * 默认数据源
     * @return
     */
    String value() default "write";

    /**
     * 清除
     * @return
     */
    boolean clear() default true;

}
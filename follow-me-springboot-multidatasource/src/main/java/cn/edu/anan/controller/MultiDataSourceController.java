package cn.edu.anan.controller;

import cn.edu.anan.entity.Order;
import cn.edu.anan.entity.User;
import cn.edu.anan.service.OrderService;
import cn.edu.anan.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 15:46
 */
@RestController
@RequestMapping("route")
@Slf4j
public class MultiDataSourceController {

    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;

    /**
     * 写数据源测试：写入一个订单
     * @param order
     * @return
     */
    @RequestMapping("write")
    public Order write(@RequestBody Order order){
        orderService.insertOne(order);
        return order;

    }

    /**
     * 读数据源测试：查询全部用户列表数据
     * @return
     */
    @RequestMapping("read")
    public List<User> read(@RequestBody User user){
        log.info("查询条件：{}", user);
        return userService.selectAll(user);
    }
}

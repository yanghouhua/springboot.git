package cn.edu.anan.entity;

import lombok.Data;

import java.util.Date;

/**
 * 订单实体
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 17:30
 */
@Data
public class Order {

    /**
     * Id
     */
    private Integer id;

    /**
     * userId
     */
    private Integer userId;

    /**
     * orderNum
     */
    private String orderNum;

    /**
     * status
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;
}

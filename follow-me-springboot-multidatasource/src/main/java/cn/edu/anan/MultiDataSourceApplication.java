package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 15:45
 */
@SpringBootApplication
@EnableTransactionManagement(order = 6)
public class MultiDataSourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiDataSourceApplication.class, args);
    }
}

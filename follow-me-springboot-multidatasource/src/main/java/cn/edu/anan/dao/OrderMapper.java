package cn.edu.anan.dao;

import cn.edu.anan.entity.Order;

/**
 * 订单mapper
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 17:34
 */
public interface OrderMapper {

    /**
     * 写入单个订单
     * @param order
     * @return
     */
    Integer insertOne(Order order);

}

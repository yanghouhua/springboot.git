package cn.edu.anan.dao;

import cn.edu.anan.entity.User;

import java.util.List;

/**
 * 用户Mapper
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/21 16:05
 */
public interface UserMapper{

    /**
     * 查询全部用户
     * @return
     */
    List<User> selectAll();

}
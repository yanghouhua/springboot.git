package cn.edu.anan.po;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * 测试线程是否安全
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/11/8 21:56
 */
@Data
@Component
public class ThreadSafePO {

    private int id;
    private String name;
}

package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/11/8 21:32
 */
@SpringBootApplication
public class BestPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BestPracticeApplication.class, args);
    }
}

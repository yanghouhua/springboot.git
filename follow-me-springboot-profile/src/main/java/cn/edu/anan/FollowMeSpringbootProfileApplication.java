package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FollowMeSpringbootProfileApplication {

	public static void main(String[] args) {
		SpringApplication.run(FollowMeSpringbootProfileApplication.class, args);
	}

}

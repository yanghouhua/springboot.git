package cn.edu.anan.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 配置类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/7/27 19:29
 */
@Component
@ConfigurationProperties(prefix = "demo")
@Data
public class DemoConfiguration {

    private String app;
    private String author;
    private String env;
}

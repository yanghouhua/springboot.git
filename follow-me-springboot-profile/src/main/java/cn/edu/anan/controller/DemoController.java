package cn.edu.anan.controller;

import cn.edu.anan.config.DemoConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * demo controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/7/27 19:27
 */
@RestController
@RequestMapping("demo")
@Slf4j
public class DemoController {

    @Autowired
    private DemoConfiguration configuration;

    @RequestMapping("ok")
    public String ok(){
        log.info("当前运行的环境是：{}，应用：{}，作者：{}",
                configuration.getEnv(),configuration.getApp(),configuration.getAuthor());

        return configuration.toString();
    }
}

# springboot项目profile特性

## 1.简要信息

在实际项目中，profile是一个非常重要的特性。举个例子，我们的项目通常都会有不同的环境

- 开发环境
- 测试环境
- 预发布环境
- 生产环境

不同的环境，相关的配置信息不同，比如数据库连接信息、redis连接信息等。

那么问题来了，如果没有profile特性，就意味着我们需要在不同的环境之间，根据需要修改配置文件内容，注意是来回频繁的修改

- 当在开发环境，需要将数据库连接改成开发库
- 当在测试环境，需要将数据库连接改成测试库
- 当在生产环境，需要将数据库连接改成生产库

来回改，要是一个不小心改错了，或者没有改，在开发环境用了生产数据库，将生产数据改乱了！谁负责！

因此，我们需要profile特性的支持！在不同的环境，使用不同的配置文件、配置内容。环境切换的时候，只需要指定激活哪一个profile即可。

springboot项目，给我们提供了profile特性，下面来看一下吧！



## 2.案例

### 2.1.准备不同环境配置文件

#### 2.1.1.公共配置属性

application.yml

```yaml
server:
  port: 8080
spring:
  application:
    name: follow-me-springboot-profile
  profiles:
    active: ${ENV:prod}
```

配置说明

- spring.profiles.active：配置当前要激活哪一个profile，支持环境变量配置



#### 2.1.2.开发环境配置

application-dev.yml

```yaml
demo:
  env: dev
  app: 开发环境应用
  author: xiaoyang
```



#### 2.1.3.生产环境配置

application-prod.yml

```yaml
demo:
  env: prod
  app: 生产环境应用
  author: xiaoyang
```



### 2.2.准备请求端点

#### 2.2.1.编写配置类

```java
@Component
@ConfigurationProperties(prefix = "demo")
@Data
public class DemoConfiguration {

    private String app;
    private String author;
    private String env;
}
```



#### 2.2.2.编写controller

```java
@RestController
@RequestMapping("demo")
@Slf4j
public class DemoController {

    @Autowired
    private DemoConfiguration configuration;

    @RequestMapping("ok")
    public String ok(){
        log.info("当前运行的环境是：{}，应用：{}，作者：{}",
                configuration.getEnv(),configuration.getApp(),configuration.getAuthor());

        return configuration.toString();
    }
}
```



### 2.3.验证

访问端点：http://127.0.0.1:8080/demo/ok

![image-20210731125148469](assets/image-20210731125148469.png)


package cn.edu.anan;

/**
 * pi Animal接口实现2
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/4 16:22
 */
public class Cat implements Animal{

    /**
     * 接口方法
     */
    @Override
    public void hello() {
        System.out.println("I am Cat.喵喵...");
    }
}

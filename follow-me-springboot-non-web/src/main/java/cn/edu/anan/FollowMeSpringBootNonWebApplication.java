package cn.edu.anan;

import cn.edu.anan.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/2 7:06
 */
@SpringBootApplication
public class FollowMeSpringBootNonWebApplication implements CommandLineRunner{

    @Autowired
    private MessageService messageService;

    /**
     * main方法
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(FollowMeSpringBootNonWebApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    /**
     * 实现run方法
     * @param args
     * @throws Exception
     */
    public void run(String... args) throws Exception {
        String message = messageService.printMessage();
        System.out.println("打印消息：" + message);
        System.exit(0);
    }
}

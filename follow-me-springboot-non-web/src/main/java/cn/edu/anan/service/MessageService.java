package cn.edu.anan.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 消息 service
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/2 7:03
 */
@Service
public class MessageService {

    @Value("${app.appId:unKnow}")
    private String appId;
    @Value("${app.appName:unKnow}")
    private String appName;

    public String printMessage(){
        return "appId:" + appId + ",appName:" +appName;
    }

}

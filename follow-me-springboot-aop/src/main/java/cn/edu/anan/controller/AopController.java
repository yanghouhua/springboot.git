package cn.edu.anan.controller;

import cn.edu.anan.aop.NeedLogin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * aop controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/8/10 20:49
 */
@RestController
@RequestMapping("aop")
@Slf4j
public class AopController {

    /**
     * 不需要登录，即可访问
     * @return
     */
    @RequestMapping("noLogin")
    public String noLogin(){
        log.info("AopController---noLogin不需要登录.");

        return "no login ok!";
    }

    /**
     * 需要登录才能访问
     * @param userId
     * @return
     */
    @RequestMapping("needLogin")
    @NeedLogin
    public String needLogin(String userId){
        log.info("AopController---needLogin.要求登录，用户Id：{}",userId);

        return "needLogin ok!";
    }
}

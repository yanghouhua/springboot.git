package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/8/10 20:47
 */
@SpringBootApplication
public class FollowMeSpringBootAopApplication {

    public static void main(String[] args) {
        SpringApplication.run(FollowMeSpringBootAopApplication.class,args);
    }
}

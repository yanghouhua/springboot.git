package cn.edu.anan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局异常处理器
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/8/10 21:09
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public Map handleGlobalException(Exception e){
        Map resultMap = new HashMap();
        resultMap.put("code", HttpStatus.UNAUTHORIZED.value());
        resultMap.put("msg", e.getMessage());

        return resultMap;

    }
}

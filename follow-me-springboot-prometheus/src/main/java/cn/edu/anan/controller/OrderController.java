package cn.edu.anan.controller;

import cn.edu.anan.service.OrderService;
import cn.edu.anan.vo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//控制器和服务类
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/order")
    public ResponseEntity<Boolean> createOrder(Order order){
        return ResponseEntity.ok(orderService.createOrder(order));
    }
}
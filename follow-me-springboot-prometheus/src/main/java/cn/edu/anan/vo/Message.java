package cn.edu.anan.vo;

import lombok.Data;

@Data
public class Message {

    private String orderId;
    private Long userId;
    private String content;
}
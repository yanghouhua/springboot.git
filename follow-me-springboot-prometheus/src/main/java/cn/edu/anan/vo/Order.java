package cn.edu.anan.vo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Order {

    private String orderId;
    private Long userId;
    private Integer amount;
    private LocalDateTime createTime;

    private String channel;
}
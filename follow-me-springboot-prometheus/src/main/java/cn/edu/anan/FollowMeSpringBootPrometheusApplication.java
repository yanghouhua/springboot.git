package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/1 19:53
 */
@SpringBootApplication
public class FollowMeSpringBootPrometheusApplication {

    public static void main(String[] args) {
        SpringApplication.run(FollowMeSpringBootPrometheusApplication.class, args);
    }
}

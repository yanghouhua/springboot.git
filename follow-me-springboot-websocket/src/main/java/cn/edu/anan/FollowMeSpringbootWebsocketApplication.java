package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FollowMeSpringbootWebsocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(FollowMeSpringbootWebsocketApplication.class, args);
	}

}

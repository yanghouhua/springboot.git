package cn.edu.anan.websocket;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 客服controller
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/5/8 20:20
 */
@Controller
@RequestMapping("/kf")
public class KfController {

    /**
     * 进入客户主页面
     * @param userId
     * @return
     */
    @GetMapping("/index/{userId}")
    public ModelAndView index(@PathVariable String userId) {
        ModelAndView mav = new ModelAndView("/index");
        mav.addObject("userId", userId);
        return mav;
    }

    /**
     * 推送数据到客户端
     * @param cid
     * @param message
     * @return
     */
    @ResponseBody
    @RequestMapping("/push/{cid}")
    public Map push(@PathVariable String cid, String message) {
        Map<String,Object> result = new HashMap<>(16);
        try {
            KfWebSocketServer.sendInfo(message, cid);
            result.put("code", cid);
            result.put("msg", message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}

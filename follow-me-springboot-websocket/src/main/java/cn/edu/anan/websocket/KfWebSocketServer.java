package cn.edu.anan.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 客服服务端：WebSocket Server
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/5/8 20:15
 */
@Component
@Slf4j
@ServerEndpoint("/kf/{sid}")
public class KfWebSocketServer {

    /**
     * 在线用户数量
     */
    private static int clientCount = 0;

    /**
     * 存放客户端连接对象
     */
    private static CopyOnWriteArraySet<KfWebSocketServer> clientSet = new CopyOnWriteArraySet<KfWebSocketServer>();

    /**
     * 客户端连接会话
     */
    private Session session;

    /**
     * 客户端标识
     */
    private String sid = "";

    /**
     * 连接建立成功调用方法
     * @param session
     * @param sid
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("sid") String sid) {
        this.session = session;
        this.sid = sid;

        // 将客户端连接，放入集合
        clientSet.add(this);
        // 客户端连接数 计数加一
        addClientCount();
        log.info("新客户建立连接标识：{}，当前在线人数：{}", sid, getClientCount());
    }

    /**
     * 连接关闭调用方法
     */
    @OnClose
    public void onClose() {
        // 从集合中移除当前客户端
        clientSet.remove(this);
        // 在线客户端数 计数减一
        subClientCount();
        log.info("客户端连接断开标识：{}，当前在线人数：{}", sid, getClientCount());
    }

    /**
     * 收到客户端信息调用方法
     * @param message
     * @param session
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("收到来自客户消息标识：{},消息：{}", sid, message);
        try {
            sendMessage("来自服务端响应：" + message);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 服务端、客户端连接发生错误
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 服务端推送消息
     * @param message
     * @throws IOException
     */
    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }

    /**
     * 将消息推送给指定客户端
     * @param message
     * @param sid
     * @throws IOException
     */
    public static void sendInfo(String message, @PathParam("sid") String sid) throws IOException {
        for (KfWebSocketServer item : clientSet) {
            try {
                if (item.sid.equals(sid)) {
                    item.sendMessage("来自服务端响应：" + message);
                    break;
                }
            } catch (IOException e) {
                continue;
            }
        }
    }

    /**
     * ====================================================================
     */
    public static synchronized int getClientCount() {
        return clientCount;
    }

    public static synchronized void addClientCount() {
        KfWebSocketServer.clientCount++;
    }

    public static synchronized void subClientCount() {
        KfWebSocketServer.clientCount--;
    }

}

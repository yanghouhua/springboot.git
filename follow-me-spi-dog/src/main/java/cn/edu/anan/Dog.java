package cn.edu.anan;

/**
 * spi Animal接口实现1
 *
 * @author ThinkPad
 * @version 1.0
 * @date 2021/9/4 16:13
 */
public class Dog implements Animal{

    /**
     * 接口方法
     */
    @Override
    public void hello() {
        System.out.println("I am Dog.汪汪...");
    }
}

package cn.edu.anan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动类
 * </p>
 *
 * @author yanghouhua@edu.com.cn
 * @since 2021/8/12
 */
@SpringBootApplication
public class FollowMeSpringBootLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(FollowMeSpringBootLogApplication.class, args);
    }
}

package cn.edu.anan.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * controller
 * </p>
 *
 * @author yanghouhua@edu.com.cn
 * @since 2021/8/12
 */
@RestController
@RequestMapping("log")
@Slf4j
public class LogController {

    @RequestMapping("test")
    public String test(){
        log.info("LogController---test.方法执行处理中.");
        log.error("错误日志输出案例.LogController---test.方法执行处理中.");
        return "log ok.";
    }

}

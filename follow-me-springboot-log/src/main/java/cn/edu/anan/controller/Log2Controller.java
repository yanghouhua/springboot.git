package cn.edu.anan.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * controller2
 * </p>
 *
 * @author yanghouhua@edu.com.cn
 * @since 2021/8/12
 */
@RestController
@RequestMapping("log")
@Slf4j
public class Log2Controller {

    @RequestMapping("test2")
    public String test2(){
        log.info("Log2Controller---test2.方法执行处理中.");
        log.error("错误日志输出案例.Log2Controller---test2.方法执行处理中.");
        return "log ok.";
    }

}
